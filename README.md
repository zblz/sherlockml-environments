![SherlockML](.images/sherlockml-logo.png)

# SherlockML environments

Want to install [RStudio Server][rstudio] or the latest
[TensorFlow][tensorflow] release candidate on your [SherlockML][sherlockml]
server? You're in the right place! This repository contains custom environments
which can be applied to your servers.

## Adding an environment to your project

Find the environment you want to add in this repository, and copy the contents
of `environment.bash` into a new script on the Environments tab of your project
on SherlockML.

![Where to paste the script](.images/environments-page.png)

Click "SAVE" to save the environment definition. You can now apply it to any
new or existing servers in your project.

## Contributing an environment

Contributions are welcome! To share your custom environment with the SherlockML
community, [fork this repository][fork], add a new subdirectory containing an
`environment.bash` file that contains the script to apply the environment, and
[open a pull request][PR].

[fork]: https://bitbucket.org/theasi/sherlockml-environments/fork
[PR]: https://bitbucket.org/theasi/sherlockml-environments/pull-requests/new
[rstudio]: https://www.rstudio.com/products/rstudio/
[sherlockml]: https://sherlockml.com
[tensorflow]: https://www.tensorflow.org/
